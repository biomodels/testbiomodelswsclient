import uk.ac.ebi.biomodels.ws.dao.client.model.ModelWsClient;
import uk.ac.ebi.biomodels.ws.dao.config.ProdConfig;
import uk.ac.ebi.biomodels.ws.dao.model.AdditionalFile;
import uk.ac.ebi.biomodels.ws.SimpleModel;
import uk.ac.ebi.biomodels.ws.dao.model.ModelFile;
import uk.ac.ebi.biomodels.ws.dao.model.ModelFiles;

public class Application {
    public static void main(String[] args) {
        ProdConfig config = new ProdConfig();
        ModelWsClient client = new ModelWsClient(config);
        String modelId = "BIOMD0000000640";
        SimpleModel model = client.getModel(modelId);
        String out = model.getName() + ": " + model.getDescription();
        System.out.println(out);
        System.out.println(client.getModelNameByModelId(modelId));
        ModelFiles files = client.getModelFiles(modelId);
        for (ModelFile mf: files.getMain()) {
            out = mf.getName() + ": " + mf.getFileSize();
            System.out.println(out);
        }

        for (AdditionalFile af: files.getAdditional()) {
            out = af.getName() + "\t" + af.getDescription() + "\t" + af.getFileSize();
            System.out.println(out);
        }
    }
}
